defmodule Skylogs.FlightsFixtures do
  @moduledoc """
  This module defines test helpers for creating
  entities via the `Skylogs.Flights` context.
  """

  @doc """
  Generate a flight.
  """
  def flight_fixture(attrs \\ %{}) do
    {:ok, flight} =
      attrs
      |> Enum.into(%{
        aircraft: :paraglider,
        description: "some description",
        endtime: ~T[14:00:00],
        flight_distance: 120.5,
        id: "7488a646-e31f-11e4-aace-600308960662",
        max_alt: 120.5,
        more_info: "some more_info",
        no_flights: 42,
        photo: "some photo",
        private_description: "some private_description",
        starttime: ~T[14:00:00],
        tracklog: "some tracklog"
      })
      |> Skylogs.Flights.create_flight()

    flight
  end

  @doc """
  Generate a flight.
  """
  def flight_fixture(attrs \\ %{}) do
    {:ok, flight} =
      attrs
      |> Enum.into(%{
        aircraft: :paraglider,
        description: "some description",
        endtime: ~T[14:00:00],
        flight_distance: 120.5,
        flight_id: "7488a646-e31f-11e4-aace-600308960662",
        max_alt: 120.5,
        more_info: "some more_info",
        no_flights: 42,
        photo: "some photo",
        private_description: "some private_description",
        starttime: ~T[14:00:00],
        tracklog: "some tracklog"
      })
      |> Skylogs.Flights.create_flight()

    flight
  end
end
