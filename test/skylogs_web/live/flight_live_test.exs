defmodule SkylogsWeb.FlightLiveTest do
  use SkylogsWeb.ConnCase

  import Phoenix.LiveViewTest
  import Skylogs.FlightsFixtures

  @create_attrs %{aircraft: :paraglider, description: "some description", endtime: "14:00", flight_distance: 120.5, flight_id: "7488a646-e31f-11e4-aace-600308960662", max_alt: 120.5, more_info: "some more_info", no_flights: 42, photo: "some photo", private_description: "some private_description", starttime: "14:00", tracklog: "some tracklog"}
  @update_attrs %{aircraft: :hangglider, description: "some updated description", endtime: "15:01", flight_distance: 456.7, flight_id: "7488a646-e31f-11e4-aace-600308960668", max_alt: 456.7, more_info: "some updated more_info", no_flights: 43, photo: "some updated photo", private_description: "some updated private_description", starttime: "15:01", tracklog: "some updated tracklog"}
  @invalid_attrs %{aircraft: nil, description: nil, endtime: nil, flight_distance: nil, flight_id: nil, max_alt: nil, more_info: nil, no_flights: nil, photo: nil, private_description: nil, starttime: nil, tracklog: nil}

  defp create_flight(_) do
    flight = flight_fixture()
    %{flight: flight}
  end

  describe "Index" do
    setup [:create_flight]

    test "lists all flights", %{conn: conn, flight: flight} do
      {:ok, _index_live, html} = live(conn, ~p"/flights")

      assert html =~ "Listing Flights"
      assert html =~ flight.description
    end

    test "saves new flight", %{conn: conn} do
      {:ok, index_live, _html} = live(conn, ~p"/flights")

      assert index_live |> element("a", "New Flight") |> render_click() =~
               "New Flight"

      assert_patch(index_live, ~p"/flights/new")

      assert index_live
             |> form("#flight-form", flight: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert index_live
             |> form("#flight-form", flight: @create_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/flights")

      html = render(index_live)
      assert html =~ "Flight created successfully"
      assert html =~ "some description"
    end

    test "updates flight in listing", %{conn: conn, flight: flight} do
      {:ok, index_live, _html} = live(conn, ~p"/flights")

      assert index_live |> element("#flights-#{flight.id} a", "Edit") |> render_click() =~
               "Edit Flight"

      assert_patch(index_live, ~p"/flights/#{flight}/edit")

      assert index_live
             |> form("#flight-form", flight: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert index_live
             |> form("#flight-form", flight: @update_attrs)
             |> render_submit()

      assert_patch(index_live, ~p"/flights")

      html = render(index_live)
      assert html =~ "Flight updated successfully"
      assert html =~ "some updated description"
    end

    test "deletes flight in listing", %{conn: conn, flight: flight} do
      {:ok, index_live, _html} = live(conn, ~p"/flights")

      assert index_live |> element("#flights-#{flight.id} a", "Delete") |> render_click()
      refute has_element?(index_live, "#flights-#{flight.id}")
    end
  end

  describe "Show" do
    setup [:create_flight]

    test "displays flight", %{conn: conn, flight: flight} do
      {:ok, _show_live, html} = live(conn, ~p"/flights/#{flight}")

      assert html =~ "Show Flight"
      assert html =~ flight.description
    end

    test "updates flight within modal", %{conn: conn, flight: flight} do
      {:ok, show_live, _html} = live(conn, ~p"/flights/#{flight}")

      assert show_live |> element("a", "Edit") |> render_click() =~
               "Edit Flight"

      assert_patch(show_live, ~p"/flights/#{flight}/show/edit")

      assert show_live
             |> form("#flight-form", flight: @invalid_attrs)
             |> render_change() =~ "can&#39;t be blank"

      assert show_live
             |> form("#flight-form", flight: @update_attrs)
             |> render_submit()

      assert_patch(show_live, ~p"/flights/#{flight}")

      html = render(show_live)
      assert html =~ "Flight updated successfully"
      assert html =~ "some updated description"
    end
  end
end
