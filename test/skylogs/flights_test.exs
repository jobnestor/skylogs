defmodule Skylogs.FlightsTest do
  use Skylogs.DataCase

  alias Skylogs.Flights

  describe "flights" do
    alias Skylogs.Flights.Flight

    import Skylogs.FlightsFixtures

    @invalid_attrs %{aircraft: nil, description: nil, endtime: nil, flight_distance: nil, id: nil, max_alt: nil, more_info: nil, no_flights: nil, photo: nil, private_description: nil, starttime: nil, tracklog: nil}

    test "list_flights/0 returns all flights" do
      flight = flight_fixture()
      assert Flights.list_flights() == [flight]
    end

    test "get_flight!/1 returns the flight with given id" do
      flight = flight_fixture()
      assert Flights.get_flight!(flight.id) == flight
    end

    test "create_flight/1 with valid data creates a flight" do
      valid_attrs = %{aircraft: :paraglider, description: "some description", endtime: ~T[14:00:00], flight_distance: 120.5, id: "7488a646-e31f-11e4-aace-600308960662", max_alt: 120.5, more_info: "some more_info", no_flights: 42, photo: "some photo", private_description: "some private_description", starttime: ~T[14:00:00], tracklog: "some tracklog"}

      assert {:ok, %Flight{} = flight} = Flights.create_flight(valid_attrs)
      assert flight.aircraft == :paraglider
      assert flight.description == "some description"
      assert flight.endtime == ~T[14:00:00]
      assert flight.flight_distance == 120.5
      assert flight.id == "7488a646-e31f-11e4-aace-600308960662"
      assert flight.max_alt == 120.5
      assert flight.more_info == "some more_info"
      assert flight.no_flights == 42
      assert flight.photo == "some photo"
      assert flight.private_description == "some private_description"
      assert flight.starttime == ~T[14:00:00]
      assert flight.tracklog == "some tracklog"
    end

    test "create_flight/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Flights.create_flight(@invalid_attrs)
    end

    test "update_flight/2 with valid data updates the flight" do
      flight = flight_fixture()
      update_attrs = %{aircraft: :hangglider, description: "some updated description", endtime: ~T[15:01:01], flight_distance: 456.7, id: "7488a646-e31f-11e4-aace-600308960668", max_alt: 456.7, more_info: "some updated more_info", no_flights: 43, photo: "some updated photo", private_description: "some updated private_description", starttime: ~T[15:01:01], tracklog: "some updated tracklog"}

      assert {:ok, %Flight{} = flight} = Flights.update_flight(flight, update_attrs)
      assert flight.aircraft == :hangglider
      assert flight.description == "some updated description"
      assert flight.endtime == ~T[15:01:01]
      assert flight.flight_distance == 456.7
      assert flight.id == "7488a646-e31f-11e4-aace-600308960668"
      assert flight.max_alt == 456.7
      assert flight.more_info == "some updated more_info"
      assert flight.no_flights == 43
      assert flight.photo == "some updated photo"
      assert flight.private_description == "some updated private_description"
      assert flight.starttime == ~T[15:01:01]
      assert flight.tracklog == "some updated tracklog"
    end

    test "update_flight/2 with invalid data returns error changeset" do
      flight = flight_fixture()
      assert {:error, %Ecto.Changeset{}} = Flights.update_flight(flight, @invalid_attrs)
      assert flight == Flights.get_flight!(flight.id)
    end

    test "delete_flight/1 deletes the flight" do
      flight = flight_fixture()
      assert {:ok, %Flight{}} = Flights.delete_flight(flight)
      assert_raise Ecto.NoResultsError, fn -> Flights.get_flight!(flight.id) end
    end

    test "change_flight/1 returns a flight changeset" do
      flight = flight_fixture()
      assert %Ecto.Changeset{} = Flights.change_flight(flight)
    end
  end

  describe "flights" do
    alias Skylogs.Flights.Flight

    import Skylogs.FlightsFixtures

    @invalid_attrs %{aircraft: nil, description: nil, endtime: nil, flight_distance: nil, flight_id: nil, max_alt: nil, more_info: nil, no_flights: nil, photo: nil, private_description: nil, starttime: nil, tracklog: nil}

    test "list_flights/0 returns all flights" do
      flight = flight_fixture()
      assert Flights.list_flights() == [flight]
    end

    test "get_flight!/1 returns the flight with given id" do
      flight = flight_fixture()
      assert Flights.get_flight!(flight.id) == flight
    end

    test "create_flight/1 with valid data creates a flight" do
      valid_attrs = %{aircraft: :paraglider, description: "some description", endtime: ~T[14:00:00], flight_distance: 120.5, flight_id: "7488a646-e31f-11e4-aace-600308960662", max_alt: 120.5, more_info: "some more_info", no_flights: 42, photo: "some photo", private_description: "some private_description", starttime: ~T[14:00:00], tracklog: "some tracklog"}

      assert {:ok, %Flight{} = flight} = Flights.create_flight(valid_attrs)
      assert flight.aircraft == :paraglider
      assert flight.description == "some description"
      assert flight.endtime == ~T[14:00:00]
      assert flight.flight_distance == 120.5
      assert flight.flight_id == "7488a646-e31f-11e4-aace-600308960662"
      assert flight.max_alt == 120.5
      assert flight.more_info == "some more_info"
      assert flight.no_flights == 42
      assert flight.photo == "some photo"
      assert flight.private_description == "some private_description"
      assert flight.starttime == ~T[14:00:00]
      assert flight.tracklog == "some tracklog"
    end

    test "create_flight/1 with invalid data returns error changeset" do
      assert {:error, %Ecto.Changeset{}} = Flights.create_flight(@invalid_attrs)
    end

    test "update_flight/2 with valid data updates the flight" do
      flight = flight_fixture()
      update_attrs = %{aircraft: :hangglider, description: "some updated description", endtime: ~T[15:01:01], flight_distance: 456.7, flight_id: "7488a646-e31f-11e4-aace-600308960668", max_alt: 456.7, more_info: "some updated more_info", no_flights: 43, photo: "some updated photo", private_description: "some updated private_description", starttime: ~T[15:01:01], tracklog: "some updated tracklog"}

      assert {:ok, %Flight{} = flight} = Flights.update_flight(flight, update_attrs)
      assert flight.aircraft == :hangglider
      assert flight.description == "some updated description"
      assert flight.endtime == ~T[15:01:01]
      assert flight.flight_distance == 456.7
      assert flight.flight_id == "7488a646-e31f-11e4-aace-600308960668"
      assert flight.max_alt == 456.7
      assert flight.more_info == "some updated more_info"
      assert flight.no_flights == 43
      assert flight.photo == "some updated photo"
      assert flight.private_description == "some updated private_description"
      assert flight.starttime == ~T[15:01:01]
      assert flight.tracklog == "some updated tracklog"
    end

    test "update_flight/2 with invalid data returns error changeset" do
      flight = flight_fixture()
      assert {:error, %Ecto.Changeset{}} = Flights.update_flight(flight, @invalid_attrs)
      assert flight == Flights.get_flight!(flight.id)
    end

    test "delete_flight/1 deletes the flight" do
      flight = flight_fixture()
      assert {:ok, %Flight{}} = Flights.delete_flight(flight)
      assert_raise Ecto.NoResultsError, fn -> Flights.get_flight!(flight.id) end
    end

    test "change_flight/1 returns a flight changeset" do
      flight = flight_fixture()
      assert %Ecto.Changeset{} = Flights.change_flight(flight)
    end
  end
end
