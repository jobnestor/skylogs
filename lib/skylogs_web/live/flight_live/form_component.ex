defmodule SkylogsWeb.FlightLive.FormComponent do
  use SkylogsWeb, :live_component

  alias Skylogs.Flights

  @impl true
  def render(assigns) do
    ~H"""
    <div>
      <.header>
        <%= @title %>
        <:subtitle>Use this form to manage flight records in your database.</:subtitle>
      </.header>

      <.simple_form
        for={@form}
        id="flight-form"
        phx-target={@myself}
        phx-change="validate"
        phx-submit="save"
      >
        <.input field={@form[:flight_id]} type="text" label="Flight" />
        <.input field={@form[:starttime]} type="time" label="Starttime" />
        <.input field={@form[:endtime]} type="time" label="Endtime" />
        <.input
          field={@form[:aircraft]}
          type="select"
          label="Aircraft"
          prompt="Choose a value"
          options={Ecto.Enum.values(Skylogs.Flights.Flight, :aircraft)}
        />
        <.input field={@form[:no_flights]} type="number" label="No flights" />
        <.input field={@form[:flight_distance]} type="number" label="Flight distance" step="any" />
        <.input field={@form[:max_alt]} type="number" label="Max alt" step="any" />
        <.input field={@form[:more_info]} type="text" label="More info" />
        <.input field={@form[:description]} type="text" label="Description" />
        <.input field={@form[:private_description]} type="text" label="Private description" />
        <.input field={@form[:tracklog]} type="text" label="Tracklog" />
        <.input field={@form[:photo]} type="text" label="Photo" />
        <:actions>
          <.button phx-disable-with="Saving...">Save Flight</.button>
        </:actions>
      </.simple_form>
    </div>
    """
  end

  @impl true
  def update(%{flight: flight} = assigns, socket) do
    changeset = Flights.change_flight(flight)

    {:ok,
     socket
     |> assign(assigns)
     |> assign_form(changeset)}
  end

  @impl true
  def handle_event("validate", %{"flight" => flight_params}, socket) do
    changeset =
      socket.assigns.flight
      |> Flights.change_flight(flight_params)
      |> Map.put(:action, :validate)

    {:noreply, assign_form(socket, changeset)}
  end

  def handle_event("save", %{"flight" => flight_params}, socket) do
    save_flight(socket, socket.assigns.action, flight_params)
  end

  defp save_flight(socket, :edit, flight_params) do
    case Flights.update_flight(socket.assigns.flight, flight_params) do
      {:ok, flight} ->
        notify_parent({:saved, flight})

        {:noreply,
         socket
         |> put_flash(:info, "Flight updated successfully")
         |> push_patch(to: socket.assigns.patch)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp save_flight(socket, :new, flight_params) do
    case Flights.create_flight(flight_params) do
      {:ok, flight} ->
        notify_parent({:saved, flight})

        {:noreply,
         socket
         |> put_flash(:info, "Flight created successfully")
         |> push_patch(to: socket.assigns.patch)}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign_form(socket, changeset)}
    end
  end

  defp assign_form(socket, %Ecto.Changeset{} = changeset) do
    assign(socket, :form, to_form(changeset))
  end

  defp notify_parent(msg), do: send(self(), {__MODULE__, msg})
end
