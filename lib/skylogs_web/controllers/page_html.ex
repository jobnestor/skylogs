defmodule SkylogsWeb.PageHTML do
  use SkylogsWeb, :html

  embed_templates "page_html/*"
end
