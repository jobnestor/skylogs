defmodule SkylogsWeb.Layouts do
  use SkylogsWeb, :html

  embed_templates "layouts/*"
end
