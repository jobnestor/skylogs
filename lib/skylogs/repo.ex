defmodule Skylogs.Repo do
  use Ecto.Repo,
    otp_app: :skylogs,
    adapter: Ecto.Adapters.Postgres
end
