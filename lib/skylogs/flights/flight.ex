defmodule Skylogs.Flights.Flight do
  use Ecto.Schema
  import Ecto.Changeset

  schema "flights" do
    field :aircraft, Ecto.Enum, values: [:paraglider, :hangglider, :speedglider, :speedrider, :sailplane]
    field :description, :string
    field :endtime, :time
    field :flight_distance, :float
    field :flight_id, Ecto.UUID
    field :max_alt, :float
    field :more_info, :string
    field :no_flights, :integer
    field :photo, :binary
    field :private_description, :string
    field :starttime, :time
    field :tracklog, :binary

    timestamps()
  end

  @doc false
  def changeset(flight, attrs) do
    flight
    |> cast(attrs, [:flight_id, :starttime, :endtime, :aircraft, :no_flights, :flight_distance, :max_alt, :more_info, :description, :private_description, :tracklog, :photo])
    |> validate_required([:flight_id, :starttime, :endtime, :aircraft, :no_flights, :flight_distance, :max_alt, :more_info, :description, :private_description, :tracklog, :photo])
  end
end
