defmodule Skylogs.Repo.Migrations.CreateFlights do
  use Ecto.Migration

  def change do
    create table(:flights) do
      add :flight_id, :uuid
      add :starttime, :time
      add :endtime, :time
      add :aircraft, :string
      add :no_flights, :integer
      add :flight_distance, :float
      add :max_alt, :float
      add :more_info, :text
      add :description, :text
      add :private_description, :text
      add :tracklog, :binary
      add :photo, :binary

      timestamps()
    end
  end
end
